# Big-Data-Spark

The aim of this project id to build a Machine Learning Model on Spark to predict the delay status of flight arrival.

**This project includes following concepts and libraries:**

- Spark Dataframes
- Spark Pipeline
- Plotly
- SparkSession
- VectorAssembler
- StringIndexer
- Model Tuning using ParamGrid
- BinaryClassificationEvaluator
- Metric Calculation
- RandomForestClassifier
- DecisionTreeClassifier
- GBTClassifier
- LogisticRegression
- Cross Validation
- Performance Metric Calculation


In addition to this, I have created several notebooks covering the basics of PySpark programming. Please find the list of notebooks below.<br/>
>>Notebooks<br/>
&ensp;&ensp;&ensp;&ensp;[1. Spark DataFrames Project Exercise.ipynb](Notebooks/1. Spark DataFrames Project Exercise.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[2. Linear Regression Project.ipynb](Notebooks/2. Linear Regression Project.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[3. Logistic Regression Project.ipynb](Notebooks/3. Logistic Regression Project.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[4. Tree Methods Project.ipynb](Notebooks/4. Tree Methods Project.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[5. Clustering Project.ipynb](Notebooks/5. Clustering Project.ipynb)<br/>


>>Spark DataFrames Basics<br/>
&ensp;&ensp;&ensp;&ensp;[1. Spark Dataframe.ipynb](Notebooks/Practice Notebooks/Spark Dataframes/1. Spark Dataframe.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[2. Spark Dataframe.ipynb](Notebooks/Practice Notebooks/Spark Dataframes/2. Spark Dataframe.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[3. Spark Dataframe.ipynb](Notebooks/Practice Notebooks/Spark Dataframes/3. Spark Dataframe.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[4. Spark Dataframe.ipynb](Notebooks/Practice Notebooks/Spark Dataframes/4. Spark Dataframe.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[5. Spark Dataframe.ipynb](Notebooks/Practice Notebooks/Spark Dataframes/5. Spark Dataframe.ipynb)<br/>

>>Spark Machine Learning Basics<br/>
&ensp;&ensp;&ensp;&ensp;[1. K-Means Clustering using Spark.ipynb](Notebooks/Practice Notebooks/Machine Learning/KMeans Clustering/1. K-Means Clustering.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[2. Linear Regression using Spark.ipynb](Notebooks/Practice Notebooks/Machine Learning/Linear Regression/1. Linear Regression using Spark MLlib.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[3. Linear Regression using Spark.ipynb](Notebooks/Practice Notebooks/Machine Learning/Linear Regression/2. Linear Regression using Spark.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[4. Logistic Regression using Spark.ipynb](Notebooks/Practice Notebooks/Machine Learning/Logistic regression/1. Logistic Regression.ipynb)<br/>
&ensp;&ensp;&ensp;&ensp;[5. Tree based classifiers using Spark.ipynb](Notebooks/Practice Notebooks/Machine Learning/Tree Based Classifiers/1. Tree based classifiers.ipynb)<br/>


